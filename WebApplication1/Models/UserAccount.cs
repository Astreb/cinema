﻿using Cinema.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.Models
{
    public class UserAccount
    {
        public User UserModel { get; set; }
        public List<Ticket> Tickets { get; set; }
        public List<Subscription> Subscriptions { get; set; }
        public bool IsCurrentUserAccount { get; set; }
    }
}
