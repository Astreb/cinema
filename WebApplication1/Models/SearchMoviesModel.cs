﻿using Cinema.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.Models
{
    public class SearchMoviesModel
    {
        public string NameFirstLetters { get; set; }
        public int ProducerId { get; set; }
        public int GenreId { get; set; }
    }
}
