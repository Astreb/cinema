﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.DataModels
{
    public class Ticket
    {
        //S.FK_HALL_ID, P.ROW, P.PLACE, M.NAME, S.DATE_
        public int Id { get; set; }
        public int FK_HallId{ get; set; }
        public int Row { get; set; }
        public int Place { get; set; }
        public string MovieName { get; set; }
        public DateTime StartDate { get; set; }
        public double Price { get; set; }

        public int? FK_SubscriptionId { get; set; }
        public int FK_UserId { get; set; }
        public int FK_PlaceID { get; set; }

        public int FK_SessionID { get; set; }

    }
}
