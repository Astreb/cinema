﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.DataModels
{
    public class Subscription
    {
        // ID, PRICE, BALANCE FROM SUBSCRIPTION
        public int Id { get; set; }
        public double Price { get; set; }
        public int Balance { get; set; }

        public int FK_UserId { get; set; }
        public int FK_SubscriptionTypeId { get; set; }

        public int AllSessionsAmount { get; set; }
    }
}
