﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.DataModels
{
    public class User
    {
        //ID, USERNAME, PASSWORD, LAST_NAME, FIRST_NAME, ACCESS_TYPE, ACCOUNT_STATUS
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int AccessType { get; set; }
        public int AccountStatus { get; set; }
        public string NormUsername { get; set; }
    }
}
