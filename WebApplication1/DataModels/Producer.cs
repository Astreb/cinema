﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.DataModels
{
    public class Producer
    {
        //ID, NAME FROM PRODUCER;
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
}
