﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.DataModels
{
    public class Log
    {
        public int Id { get; set; }
        public string OldValue { get; set; }
        public DateTime Date { get; set; }
        public string UserName { get; set; }
        public string ActionName { get; set; }
    }
}
