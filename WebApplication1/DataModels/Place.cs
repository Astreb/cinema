﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.DataModels
{
    public class Place
    {
        //PLACE.ID, PLACE.ROW, PLACE.PLACE FROM 
        public int Id { get; set; }
        public int Row { get; set; }
        public double Price { get; set; }
        public int PlaceNumber { get; set; }
    }
}
