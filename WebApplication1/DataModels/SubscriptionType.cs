﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.DataModels
{
    public class SubscriptionType
    {
        //ID, PRICE, SESSIONS_COUNT
        public int Id { get; set; }
        public double Price { get; set; }
        public int SessionCount { get; set; }
        public bool Deactivated { get; set; }

    }
}
