﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cinema.DataModels
{
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Country { get; set; }

        [Required]
        public string Date { get; set; }

        
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }

        [Required]
        public double Price { get; set; }

        [Required]
        public int Duration { get; set; }

        public List<Producer> Producers { get; set; }
        public List<Genre> Genres { get; set; }
    }
}
