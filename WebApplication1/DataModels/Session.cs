﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.DataModels
{
    public class Session
    {
        //S.ID, S.FK_HALL_ID, M.NAME, M.DURATION, S.DATE_
        public int Id { get; set; }
        public int FK_HallId { get; set; }

        public int MovieId { get; set; }
        public string MovieName { get; set; }
        public int Duration { get; set; }
        public DateTime StartTime { get; set; }

        public double Price { get; set; }

    }
}
