﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Authorization;
using Cinema.Contracts;
using Cinema.DataModels;
using Cinema.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Controllers
{
    public class UserAccountController : Controller
    {
        IRepository _repo;
        public UserAccountController(IRepository repository, UserManager<User> userManager)//, SignInManager<User> signInManager)
        {
            _repo = repository;

            _userManager = userManager;
           // _signInManager = signInManager;
        }

        public IActionResult MyAccount()
        {
            return RedirectToAction("Index", new { userid = User.GetId() });
        }

        public IActionResult Index(int userid)
        {
            var account = new UserAccount();
            account.UserModel = _repo.GetUserById(userid);
            account.Tickets = _repo.GetUserTickets(userid).ToList();
            account.Subscriptions = _repo.GetUserSubscriptions(userid).ToList();
            account.IsCurrentUserAccount = userid == User.GetId();
            ViewBag.IsAdmin = User.HasClaim("AccessType", "Admin");
            
            return View("UserAccount", account);

        }

        [HttpPost]
        public IActionResult Delete(int userid)
        {
            _repo.DeactivateUser(userid);
            return RedirectToAction("Index", new { userid = userid });
        }

        [HttpGet]
        public IActionResult Edit(int userid)
        {
            var user = _repo.GetUserById(userid);
            return View("EditUser", user);
        }

        [HttpPost]
        public async Task<IActionResult> Save(User user, string currentpassword, string newPassword)
        {
            if(string.IsNullOrWhiteSpace(newPassword) && !string.IsNullOrWhiteSpace(newPassword))
            {

                    ModelState.AddModelError("Password", "new password cannot be empty");
                    return View("EditUser", user);        
            }
            if (newPassword != currentpassword)
            {
                var u = _repo.GetUserById(user.Id);
                if(u.Password == currentpassword)
                user.Password = newPassword;
                else
                {
                    ModelState.AddModelError("Password", "Wrong password");
                    return View("EditUser", user);
                }
            }
            
            _repo.UpdateUser(user);
           // await _userManager.UpdateAsync(user);
            return RedirectToAction("Index", new { userid = user.Id });
        }

        [HttpPost]
        public IActionResult Cancel(User user)
        {
            return RedirectToAction("Index", new { userid = user.Id });
        }

        private readonly UserManager<User> _userManager;

        [HttpGet]
        [Route("/Account/Login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Login", "UserAccount");
        }

        [HttpPost]
        [Route("/Account/Login")]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                if (user == null)
                {
                    ModelState.AddModelError("", "User doesn't exist");
                    return View(model);
                }

                if(!await _userManager.CheckPasswordAsync(user, model.Password))
                {
                    ModelState.AddModelError("", "Password is incorrect");
                    return View(model);
                }

                var identity = new ClaimsIdentity("cookies");
                identity.AddClaim(new Claim("Id", user.Id.ToString()));
                identity.AddClaim(new Claim("AccessType", user.AccessType == 0 ? "Admin" : "Customer"));

                await HttpContext.SignInAsync(new ClaimsPrincipal(identity));

                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User { UserName = model.Username, Password = model.Password, AccessType = model.Admin ? 0: 1 };

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(model);
        }

    }
}
