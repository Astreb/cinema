﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cinema.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Controllers
{
    public class LogController : Controller
    {
        IRepository _repo;
        public LogController(IRepository repository)
        {
            _repo = repository;
        }

        public IActionResult Index(int userid)
        {
            
            var logs = _repo.GetUserLog(userid);
            var res = logs.Select(x =>
            {
                var oldVal = x.OldValue == null ? string.Empty : $"old value is {x.OldValue}";
                var str = $"On |{x.Date}| - User |{ x.UserName }| performed action |{x.ActionName}|   {oldVal}";
                return str;
            });
            return View("Log", res);
        }
    }
}