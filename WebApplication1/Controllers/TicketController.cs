﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authorization;
using Cinema.Contracts;
using Cinema.DataModels;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Controllers
{
    public class TicketController : Controller
    {   
        IRepository _repo;
        public TicketController(IRepository repository)
        {
            _repo = repository;
        }

        [HttpGet]
        public IActionResult BuyTicket(int sessionid)
        {
            ViewBag.userid = User.GetId();
            var ticket = new Ticket();
            ticket.FK_SessionID = sessionid;
            return View("PurchaseTicket", ticket);
        }

        [HttpPost]
        public IActionResult Save(Ticket ticket)
        {
            ticket.FK_UserId = User.GetId();
            
            var price = _repo.CalculatePrice(ticket.FK_SessionID, ticket.FK_PlaceID);
            ticket.Price = price;
            
            if (ticket.FK_SubscriptionId != 0 && ticket.FK_SubscriptionId != null)
            {
                _repo.DecreaseSessionsCount((int)ticket.FK_SubscriptionId);
            }
            else 
                ticket.FK_SubscriptionId = -1;

            _repo.AddTicket(ticket);
            return RedirectToAction("Search", "Movie");

        }

        [HttpPost]
        public IActionResult Delete(int ticketid)
        {
            var subscrId = _repo.GetSubscriptionByTicketId(ticketid);
            if(subscrId != null)
            {
                _repo.IncreaseSessionsCount((int)subscrId);
            }
            _repo.DeleteTicket(ticketid);
            return RedirectToAction("Index", "UserAccount", new { userid = User.GetId()});
        }
    }
}