﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cinema.Contracts;
using Cinema.DataModels;
using Cinema.Models;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Controllers
{
    public class SessionController : Controller
    {
        private enum TimeAllowance
        {
            Morning = 3,
            Day = 4,
            Evening = 5
        }

        private int GetTimeAllowance(DateTime dateTime)
        {
            if (dateTime.TimeOfDay.Hours < 14)
            {
                return (int)TimeAllowance.Morning;
            }
            else if (dateTime.TimeOfDay.Hours < 18)
            {
                return (int)TimeAllowance.Day;
            }
            return (int)TimeAllowance.Evening;
        }

        IRepository _repository;
        public SessionController(IRepository repository)
        {
            _repository = repository;
        }

        public IActionResult Add(int movieid)
        {
            return View("AddSession", new Session { MovieId = movieid });
        }

        [HttpPost]
        public IActionResult Save(Session session)
        {
            session.Price = _repository.GetPriceByTimeAllowance(GetTimeAllowance(session.StartTime));
            _repository.AddSession(session);
            return RedirectToAction("Search");
        }

        [HttpGet]
        public IActionResult Search()
        {
            return View("SearchSessions", new SearchSessionsModel());
        }

        [HttpPost]
        public IActionResult Search(SearchSessionsModel searchSessionsModel)
        {
            ViewBag.IsAdmin = User.HasClaim("AccessType", "Admin");
            if(searchSessionsModel.Date == null)
            {
                return View("SessionList", _repository.GetAllSessions());
            }
            return View("SessionList", _repository.GetSessionsByDate(searchSessionsModel.Date.Trim()));
        }

        [HttpGet]
        public IActionResult SearchByFilm(int movieId)
        {
            ViewBag.IsAdmin = User.HasClaim("AccessType", "Admin");
            return View("SessionList", _repository.GetSessionsByMovie(movieId));
        }

        [HttpGet]
        public IActionResult Cancel()
        {
            return RedirectToAction("Search");
        }

        [HttpPost]
        public IActionResult Delete(int sessionid)
        {
            var tickets = _repository.GetSessionTickets(sessionid);
            foreach(var t in tickets)
            {
                if(t.FK_SubscriptionId != null)
                {
                    _repository.IncreaseSessionsCount((int)t.FK_SubscriptionId);
                }
                _repository.DeleteTicket(t.Id);
            }
            _repository.DeleteSession(sessionid);
            return new EmptyResult();
        }
    }
}