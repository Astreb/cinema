﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Cinema.DataModels;
using Cinema.Contracts;
using Cinema.Models;
using System;
using System.Collections.Generic;

namespace Cinema.Controllers
{
    public class MovieController : Controller
    {
        private readonly IRepository _repo;
        public MovieController(IRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View("EditMovie", new Movie());
        }

        [HttpGet]
        public IActionResult Edit(int movieId)
        {
            var movie = _repo.GetMovieById(movieId);
            return View("EditMovie", movie);
        }

        [HttpPost]
        public IActionResult Save(Movie movie, string producersStr, List<Genre> test)
        {

            if(!ModelState.IsValid)
            {
                return View("EditMovie", movie);
            }

            DateTime releasedate;
            var date = movie.Date?.Trim();
            try
            { 
                releasedate = new DateTime(Convert.ToInt32(date.Substring(6, 4)), Convert.ToInt32(date.Substring(3, 2)), Convert.ToInt32(date.Substring(0, 2)));
                if (releasedate < DateTime.Now)
                    throw new Exception();
            }
            catch
            {
                ModelState.AddModelError("Date", "Entered release date is not valid date");
                return View("EditMovie", movie);
            }

            movie.ReleaseDate = releasedate;
            movie.Genres = movie.Genres.Where(x => x.Selected).ToList();
            
            if(!string.IsNullOrWhiteSpace(producersStr))
            {
                var producers = producersStr.Split(',', StringSplitOptions.RemoveEmptyEntries);
                for(int i = 0; i < producers.Length; ++ i)
                {
                    if(movie.Producers.Any(x => x.Name == producers[i]))
                    {
                        continue;
                    }

                    var prod = _repo.GetProducerIdByName(producers[i].Trim());
                    if (prod == null)
                    {
                        prod = _repo.AddProducer(producers[i].Trim());
                    }

                    movie.Producers.Add(new Producer { Id = (int)prod, Name = producers[i] });
                }
            }

            
            if(movie.Id > 0)
            {
                _repo.UpdateMovie(movie);
            }
            else
            {
                _repo.AddMovie(movie);
            }
            return View("SearchMovies");
        }

        [HttpPost]
        public IActionResult Delete(Movie movie)
        {
            var tickets = _repo.GetMovieTickets(movie.Id);
            foreach(var t in tickets)
            {
                if(t.FK_SubscriptionId != null)
                {
                    _repo.DeleteSubscription((int)t.FK_SubscriptionId);
                }
                _repo.DeleteTicket(t.Id);
            }

            var sessions = _repo.GetSessionsByMovie(movie.Id);
            foreach(var s in sessions)
            {
                _repo.DeleteSession(s.Id);
            }
            _repo.DeleteMovie(movie.Id);
            return View("SearchMovies");
        }

        [HttpGet]
        public IActionResult Search()
        {
            return View("SearchMovies", new SearchMoviesModel());
        }

        [HttpPost]
        public IActionResult Cancel()
        {
            return RedirectToAction("Search");
        }

        [HttpPost]
        public IActionResult Search(SearchMoviesModel model)
        {
            ViewBag.IsAdmin = User.HasClaim("AccessType", "Admin");
            IEnumerable<Movie> movies;
            if (ViewBag.IsAdmin)
            { movies = _repo.GetMoviesByNameGenreOrProducer(model.NameFirstLetters, model.GenreId, model.ProducerId).ToList(); }
            else
            {
                movies = _repo.GetCurrentMovies(DateTime.Now, model.NameFirstLetters, model.GenreId, model.ProducerId).ToList();
            }
            return View("MovieList", movies);
        }
    }
}