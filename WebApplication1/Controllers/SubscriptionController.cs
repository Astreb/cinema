﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authorization;
using Cinema.Contracts;
using Cinema.DataModels;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Controllers
{
    public class SubscriptionController : Controller
    {
        IRepository _repo;
        public SubscriptionController(IRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public IActionResult Purchase()
        {
            return View("PurchaseSubscription", new Subscription());
        }

        [HttpPost]
        public IActionResult Save(Subscription subscription, string typeInfo)
        {
            var v = typeInfo.Split('#');
            var id = Convert.ToInt32(v[0]);
            var price = Convert.ToDouble(v[1]);
            var amount = Convert.ToInt32(v[2]);
            subscription.FK_UserId = User.GetId();
            subscription.FK_SubscriptionTypeId = id;
            subscription.Price = price;
            subscription.AllSessionsAmount = amount;
            subscription.Balance = amount;

            _repo.AddSubscription(subscription);
            return RedirectToAction("Index", "UserAccount", new { userid= User.GetId() });

        }
        [HttpGet]
        public IActionResult Manage()
        {
            var types = _repo.GetSubscriptionTypes();
            return View("SubscrTypes", types);
        }

        [HttpPost]
        public IActionResult Return(int subscrid)
        {
            _repo.DeleteSubscription(subscrid);
            return new EmptyResult();
        }

        [HttpPost]
        public IActionResult DeleteType(int subscrid)
        {
            _repo.DeleteSubscriptionType(subscrid);
            return new EmptyResult();
        }

        [HttpGet]
        public IActionResult AddType()
        {
            var type = new SubscriptionType();
            return View("AddSubscrType", type);
        }

        [HttpPost]
        public IActionResult SaveType(SubscriptionType type)
        {
            _repo.AddSubscriptionType(type);
            return RedirectToAction("Manage");
        }

        [HttpPost]
        public IActionResult CancelType()
        {
            return RedirectToAction("Manage");
        }

        [HttpPost]
        public IActionResult Cancel()
        {
            return RedirectToAction("MyAccount", "UserAccount");
        }
    }
}