﻿using System;
using System.Linq;
using System.Security.Claims;


namespace Authorization
{
    public static class ClaimsPrincipalExtensions
    {
        public static int GetId(this ClaimsPrincipal claimsPrincipal)
        {
            var userId = claimsPrincipal.Claims
                .Where(c => c.Type == AuthorizationConstants.IdClaim)
                .Select(c => c.Value).SingleOrDefault();
            return Convert.ToInt32(userId);
        }
    }
}
