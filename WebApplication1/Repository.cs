﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Cinema.Contracts;
using Cinema.DataModels;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;

namespace Cinema
{
    public class Repository : IRepository
    {
        private readonly SqlConnection _sqlConnection;
        public Repository(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
            if (sqlConnection.State != System.Data.ConnectionState.Open)
                _sqlConnection.Open();
        }

        //+
        public void AddMovie(Movie movie)
        {
            var query = @"INSERT INTO MOVIE(NAME, COUNTRY, DATE_, PRICE, DURATION) VALUES(@name, @country, @date_, @price, @duration) SELECT Output = Scope_Identity()";

            var genreInsert = "INSERT INTO GENRE_MOVIE (FK_MOVIE_ID, FK_GENRE_ID) VALUES (@FK_MOVIE_ID, @FK_GENRE_ID)";
            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@name", movie.Name);
            sqlCommand.Parameters.AddWithValue("@country", (object)movie.Country ?? DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@date_", movie.ReleaseDate);
            sqlCommand.Parameters.AddWithValue("@price", movie.Price);
            sqlCommand.Parameters.AddWithValue("@duration", movie.Duration);

            var reader = sqlCommand.ExecuteReader();
            decimal createdMovieId;
            if (reader.Read())
            {
                createdMovieId = System.Convert.ToInt32(reader.GetValue(0));
            }
            else
            {
                throw new Exception();
            }
            reader.Close();

            for (int i = 0; i < movie.Genres.Count; i++)
            {
                var genrecom = _sqlConnection.CreateCommand();
                genrecom.CommandText = genreInsert;

                genrecom.Parameters.AddWithValue("@FK_MOVIE_ID", createdMovieId);
                genrecom.Parameters.AddWithValue("@FK_GENRE_ID", movie.Genres[i].Id);
                genrecom.ExecuteNonQuery();
            }

            for (int i = 0; i < movie.Producers.Count; i++)
            {
                var prodInsert = @"INSERT INTO PRODUCER_MOVIE (FK_MOVIE_ID, FK_PRODUCER_ID) VALUES (@FK_MOVIE_ID, @FK_PRODUCER_ID)";
                var prodCom = _sqlConnection.CreateCommand();
                prodCom.CommandText = prodInsert;
                prodCom.Parameters.AddWithValue("@FK_MOVIE_ID", createdMovieId);
                prodCom.Parameters.AddWithValue("@FK_PRODUCER_ID", movie.Producers[i].Id);


                prodCom.ExecuteNonQuery();
            }

        }

        // +
        public int AddProducer(string name)
        {
            var query = "INSERT INTO PRODUCER (NAME) VALUES (@NAME) SELECT Output = Scope_Identity()";
            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@NAME", name);

            var reader = sqlCommand.ExecuteReader();
            decimal createdProdId;
            if (reader.Read())
            {
                return System.Convert.ToInt32(reader.GetValue(0));
            }
            else
            {
                throw new Exception();
            }
        }

        // +
        public void AddSession(Session session)
        {
            var query = "INSERT INTO SESSION_ (DATE_, FK_MOVIE_ID, FK_HALL_ID, PRICE) VALUES (@DATE_, @FK_MOVIE_ID, @FK_HALL_ID, @PRICE)";
            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@DATE_", session.StartTime);
            sqlCommand.Parameters.AddWithValue("@FK_MOVIE_ID", session.MovieId);
            sqlCommand.Parameters.AddWithValue("@FK_HALL_ID", session.FK_HallId);
            sqlCommand.Parameters.AddWithValue("@PRICE", session.Price);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public void AddSubscription(Subscription subscription)
        {
            var query = @"INSERT INTO SUBSCRIPTION(PRICE, BALANCE, FK_SUBSCRIPTION_TYPE_ID, FK_USER_ID, DEACTIVATED)
                       VALUES(@PRICE, @BALANCE, @FK_SUBSCRIPTION_TYPE_ID, @FK_USER_ID, 0)";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@PRICE", subscription.Price);
            sqlCommand.Parameters.AddWithValue("@BALANCE", subscription.Balance);
            sqlCommand.Parameters.AddWithValue("@FK_SUBSCRIPTION_TYPE_ID", subscription.FK_SubscriptionTypeId);
            sqlCommand.Parameters.AddWithValue("@FK_USER_ID", subscription.FK_UserId);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public void AddSubscriptionType(SubscriptionType subscriptionType)
        {
            var query = "INSERT INTO SUBSCRIPTION_TYPE (PRICE, SESSIONS_COUNT, DEACTIVATED) VALUES (@PRICE, @SESSIONS, 0)";
            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@PRICE", subscriptionType.Price);
            sqlCommand.Parameters.AddWithValue("@SESSIONS", subscriptionType.SessionCount);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public void AddTicket(Ticket ticket)
        {
            var query = @"INSERT INTO TICKET (PRICE, BUYING_TIME, FK_SUBSCRIPTION_ID, FK_USER_ID, FK_PLACE_ID, FK_SESSION_ID) 
                    VALUES (@PRICE, @BUYING_TIME, @FK_SUBSCRIPTION_ID, @FK_USER_ID, @FK_PLACE_ID, @FK_SESSION_ID)";
            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@PRICE", ticket.Price);
            sqlCommand.Parameters.AddWithValue("@BUYING_TIME", DateTime.Now);
            if (ticket.FK_SubscriptionId == -1)
                sqlCommand.Parameters.AddWithValue("@FK_SUBSCRIPTION_ID", DBNull.Value);
            else
                sqlCommand.Parameters.AddWithValue("@FK_SUBSCRIPTION_ID", ticket.FK_SubscriptionId);
            sqlCommand.Parameters.AddWithValue("@FK_USER_ID", ticket.FK_UserId);
            sqlCommand.Parameters.AddWithValue("@FK_PLACE_ID", ticket.FK_PlaceID);
            sqlCommand.Parameters.AddWithValue("@FK_SESSION_ID", ticket.FK_SessionID);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public void AddTimeAllowance(TimeAllowance timeAllowance)
        {
            var query = "INSERT INTO TIME_ALLOWANCE(START_TIME, END_TIME, PRICE) VALUES(@START_TIME, @END_TIME, @PRICE)";
            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@START_TIME", timeAllowance.StartTime);
            sqlCommand.Parameters.AddWithValue("@END_TIME", timeAllowance.EndTime);
            sqlCommand.Parameters.AddWithValue("@PRICE", timeAllowance.Price);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public void AddUser(User user)
        {
            var query = @"INSERT INTO USER_ (USERNAME, PASSWORD, LAST_NAME, FIRST_NAME, ACCESS_TYPE, ACCOUNT_STATUS, NORM_USERNAME)
                VALUES(@USERNAME, @PASSWORD, @LAST_NAME, @FIRST_NAME, @ACCESS_TYPE, @ACCOUNT_STATUS, @NORM_USERNAME)";
            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@USERNAME", user.UserName);
            sqlCommand.Parameters.AddWithValue("@PASSWORD", user.Password);
            sqlCommand.Parameters.AddWithValue("@LAST_NAME", (object)user.LastName ?? DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@FIRST_NAME", (object)user.FirstName ?? DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@ACCESS_TYPE", user.AccessType);
            sqlCommand.Parameters.AddWithValue("@ACCOUNT_STATUS", 1);
            sqlCommand.Parameters.AddWithValue("@NORM_USERNAME", user.NormUsername);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public double CalculatePrice(int sessionid, int placeid)
        {
            var query = @"select (select PRICE from PLACE where ID = @placeid) + s.PRICE
                        from SESSION_ s
                        where s.ID = @sessionid";

            var sqlcommand = _sqlConnection.CreateCommand();
            sqlcommand.CommandText = query;
            sqlcommand.Parameters.AddWithValue("@placeid", placeid);
            sqlcommand.Parameters.AddWithValue("@sessionid", sessionid);

            return (System.Single)sqlcommand.ExecuteScalar();
        }

        public Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
        {
            AddUser(user);
            return Task.FromResult(IdentityResult.Success);
        }

        // +
        public void DecreaseSessionsCount(int subscriptionId)
        {
            UpdateSubscription(subscriptionId, -1);
        }

        public Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
        {
            DeactivateUser(user.Id);
            return Task.FromResult(IdentityResult.Success);
        }

        // +
        public void DeleteMovie(int id)
        {
            var query = @"DELETE FROM MOVIE M WHERE M.ID = @ID";

            var relatedObjsDeleteQuery = @"DELETE FROM PRODUCER_MOVIE PM WHERE PM.FK_MOVIE_ID = @ID;
                                           DELETE FROM GENRE_MOVIE GM WHERE GM.FK_MOVIE_ID = @ID";
            var sqlCommand = _sqlConnection.CreateCommand();

            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@ID", id);
            sqlCommand.ExecuteNonQuery();

            var relObjsCommand = _sqlConnection.CreateCommand();
            relObjsCommand.CommandText = relatedObjsDeleteQuery;
            relObjsCommand.Parameters.AddWithValue("@ID", id);
            relObjsCommand.ExecuteNonQuery();
        }

        // +
        public void DeleteSession(int id)
        {
            var query = @"DELETE FROM SESSION_ WHERE ID = @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@ID", id);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public void DeleteSubscription(int id)
        {
            var query = @"UPDATE SUBSCRIPTION set DEACTIVATED = 1 WHERE ID = @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@ID", id);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public void DeleteTicket(int id)
        {
            var query = @"DELETE FROM TICKET WHERE ID = @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@ID", id);

            sqlCommand.ExecuteNonQuery();
        }

        public void Dispose()
        {

        }

        public Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return Task.FromResult(GetUserById(Convert.ToInt32(userId)));
        }

        public Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            var query = @"SELECT ID, USERNAME, PASSWORD, LAST_NAME, FIRST_NAME, ACCESS_TYPE, ACCOUNT_STATUS
                        FROM USER_
                        WHERE NORM_USERNAME = @USERNAME";//надо погуглить как искать с IgnoreCase

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@USERNAME", normalizedUserName);

            User user = new User();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int index = reader.GetOrdinal("ID");
                        user.Id = reader.GetInt32(index);
                        index = reader.GetOrdinal("USERNAME");
                        user.UserName = reader.GetString(index);
                        index = reader.GetOrdinal("PASSWORD");
                        user.Password = reader.GetString(index);
                        index = reader.GetOrdinal("FIRST_NAME");
                        user.FirstName = GetValueOrNull<string>(reader, index);//reader.GetString(index);
                        index = reader.GetOrdinal("LAST_NAME");
                        user.LastName = GetValueOrNull<string>(reader, index);//reader.GetString(index);
                        index = reader.GetOrdinal("ACCESS_TYPE");
                        user.AccessType = reader.GetInt32(index);
                        index = reader.GetOrdinal("ACCOUNT_STATUS");
                        user.AccountStatus = reader.GetInt32(index);
                    }
                }
            }

            return Task.FromResult(user);
        }

        // +
        public IEnumerable<int> GetAllHalls()
        {
            var query = @"SELECT HALL.ID, HALL.PLACE_COUNT FROM HALL";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;

            List<int> result = new List<int>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int hall = reader.GetInt32(0);
                        result.Add(hall);
                    }
                }
            }

            return result;
        }

        // +
        public IEnumerable<Movie> GetCurrentMovies(DateTime now, string name, int genreId, int producerId)
        {
            var datetime = now.ToString("yyyy-MM-dd HH:mm:ss");
            var query = $@"SELECT distinct M.ID, M.NAME, M.COUNTRY, M.DATE_, M.DURATION
                        FROM MOVIE M
                        left outer JOIN GENRE_MOVIE GM
                        ON GM.FK_MOVIE_ID = M.ID
                        left outer JOIN GENRE G
                        ON GM.FK_GENRE_ID = G.ID
                        left outer JOIN PRODUCER_MOVIE PM
                        ON PM.FK_MOVIE_ID = M.ID
                        left outer JOIN PRODUCER P
                        ON PM.FK_PRODUCER_ID = P.ID
                        WHERE M.NAME LIKE '{name}%'
                        and (P.ID = @pid or @pid = 0) and (G.ID = @gid or @gid = 0)";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            //sqlCommand.Parameters.AddWithValue("@date_", datetime);
            sqlCommand.Parameters.AddWithValue("@pid", producerId);
            sqlCommand.Parameters.AddWithValue("@gid", genreId);

            List<Movie> result = new List<Movie>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Movie movie = new Movie();
                        movie.Id = reader.GetInt32(0);
                        movie.Name = reader.GetString(1);
                        movie.Country = GetValueOrNull<string>(reader, 2);
                        movie.Duration = reader.GetInt32(4);
                        movie.ReleaseDate = reader.GetDateTime(3);
                        movie.Genres = GetMovieGenres(movie.Id).ToList();
                        movie.Producers = GetMovieProducers(movie.Id).ToList();

                        if (movie.ReleaseDate >= now) 
                            result.Add(movie);
                    }
                }
            }

            return result;
        }

        // +
        public IEnumerable<Place> GetFreePlacesForSession(int sessionId)
        {
            var query = @"SELECT PLACE.ID, PLACE.ROW_, PLACE.PLACE FROM PLACE
                        WHERE PLACE.ID NOT IN
                        (SELECT FK_PLACE_ID
                        FROM TICKET WHERE
                        FK_SESSION_ID = @SESSION_ID)";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@SESSION_ID", sessionId);

            List<Place> result = new List<Place>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Place place = new Place();
                        place.Id = reader.GetInt32(0);
                        place.Row = reader.GetInt32(1);
                        place.PlaceNumber = reader.GetInt32(2);
                        result.Add(place);
                    }
                }
            }

            return result;
        }

        // +
        public int? GetGenreIdByName(string name)
        {
            var query = @"SELECT G.ID FROM GENRE G
                          WHERE G.NAME = @NAME";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@NAME", name);

            List<int> result = new List<int>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        return reader.GetInt32(0);
                    }
                }
            }

            return null;
        }

        // +
        public IEnumerable<Genre> GetGenres()
        {
            var query = @"SELECT ID, NAME FROM GENRE";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;

            List<Genre> result = new List<Genre>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Genre genre = new Genre();
                        genre.Id = reader.GetInt32(0);
                        genre.Name = reader.GetString(1);
                        result.Add(genre);
                    }
                }
            }

            return result;
        }

        public IEnumerable<Producer> GetProducers()
        {
            var query = @"SELECT ID, NAME FROM PRODUCER";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;

            List<Producer> result = new List<Producer>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Producer producer = new Producer();
                        producer.Id = reader.GetInt32(0);
                        producer.Name = reader.GetString(1);
                        result.Add(producer);
                    }
                }
            }

            //result.Add(new Producer { Id = 1, Name = "Joe Russo" });
            //result.Add(new Producer { Id = 2, Name = "Anthony Russo" });
            //result.Add(new Producer { Id = 3, Name = "Luc Besson" });

            return result;
        }

        // + 
        public Movie GetMovieById(int id)
        {
            var query = @"SELECT M.ID, M.NAME, M.COUNTRY, M.DATE_, M.DURATION, M.PRICE
                        FROM MOVIE M
                        left outer JOIN GENRE_MOVIE GM
                        ON GM.FK_MOVIE_ID = M.ID
                        left outer JOIN GENRE G
                        ON GM.FK_GENRE_ID = G.ID
                        left outer JOIN PRODUCER_MOVIE PM
                        ON PM.FK_MOVIE_ID = M.ID
                        left outer JOIN PRODUCER P
                        ON PM.FK_PRODUCER_ID = P.ID
                        WHERE M.ID = @id";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@id", id);

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Movie movie = new Movie();
                        movie.Id = reader.GetInt32(0);
                        movie.Name = reader.GetString(1);
                        movie.Country = GetValueOrNull<string>(reader, 2);
                        movie.Duration = reader.GetInt32(4);
                        movie.ReleaseDate = reader.GetDateTime(3);
                        movie.Genres = GetMovieGenres(movie.Id).ToList();
                        movie.Producers = GetMovieProducers(movie.Id).ToList();
                        movie.Price = reader.GetFloat(5);
                        return movie;
                    }
                }
            }

            return null;
        }

        // +
        public IEnumerable<Producer> GetMovieProducers(int movieId)
        {
            var query = @"SELECT P.ID, P.NAME FROM PRODUCER P
                        INNER JOIN PRODUCER_MOVIE PM ON PM.FK_PRODUCER_ID = P.ID
						where PM.FK_MOVIE_ID = @FK_MOVIE_ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@FK_MOVIE_ID", movieId);

            List<Producer> result = new List<Producer>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Producer producer = new Producer();
                        producer.Id = reader.GetInt32(0);
                        producer.Name = reader.GetString(1);
                        result.Add(producer);
                    }
                }
            }

            return result;
        }

        // +
        public IEnumerable<Movie> GetMoviesByGenre(int genreId)
        {
            var query = @"SELECT M.ID, M.NAME, M.COUNTRY, M.DATE_, M.DURATION
                        FROM MOVIE M
                        JOIN GENRE_MOVIE GM
                        ON GM.FK_MOVIE_ID = M.ID
                        JOIN GENRE G
                        ON GM.FK_GENRE_ID = G.ID
                        JOIN PRODUCER_MOVIE PM
                        ON PM.FK_MOVIE_ID = M.ID
                        JOIN PRODUCER P
                        ON PM.FK_PRODUCER_ID = P.ID
                        WHERE G.ID = @id";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@id", genreId);

            List<Movie> result = new List<Movie>();

            using (SqlDataReader reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Movie movie = new Movie();
                        movie.Id = reader.GetInt32(0);
                        movie.Name = reader.GetString(1);
                        movie.Country = GetValueOrNull<string>(reader, 2);
                        movie.Duration = reader.GetInt32(4);
                        movie.ReleaseDate = reader.GetDateTime(3);
                        movie.Genres = GetMovieGenres(movie.Id).ToList();
                        movie.Producers = GetMovieProducers(movie.Id).ToList();
                        result.Add(movie);
                    }
                }
            }

            return result;
        }

        // +
        public IEnumerable<Movie> GetMoviesByNameGenreOrProducer(string name, int genreId, int producerId)
        {
            var query = $@"SELECT Distinct M.ID, M.NAME, M.COUNTRY, M.DATE_, M.DURATION
                        FROM MOVIE M
                        left outer JOIN GENRE_MOVIE GM
                        ON GM.FK_MOVIE_ID = M.ID
                        left outer JOIN GENRE G
                        ON GM.FK_GENRE_ID = G.ID
                        left outer JOIN PRODUCER_MOVIE PM
                        ON PM.FK_MOVIE_ID = M.ID
                        left outer JOIN PRODUCER P
                        ON PM.FK_PRODUCER_ID = P.ID
                        WHERE M.NAME LIKE '{name}%'
                        and (P.ID = @pid or @pid = 0) and (G.ID = @gid or @gid = 0)";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@pid", producerId);
            sqlCommand.Parameters.AddWithValue("@gid", genreId);

            List<Movie> result = new List<Movie>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    Movie movie = new Movie();
                    movie.Id = reader.GetInt32(0);
                    movie.Name = reader.GetString(1);
                    movie.Country = GetValueOrNull<string>(reader, 2);
                    movie.Duration = reader.GetInt32(4);
                    movie.ReleaseDate = reader.GetDateTime(3);
                    movie.Genres = GetMovieGenres(movie.Id).ToList();
                    movie.Producers = GetMovieProducers(movie.Id).ToList();
                    result.Add(movie);
                }
            }
            return result;
        }

        // +
        public IEnumerable<Movie> GetMoviesByProducer(int producerId)
        {
            var query = @"SELECT M.ID, M.NAME, M.COUNTRY, M.DATE_, M.DURATION
                        FROM MOVIE M
                        JOIN GENRE_MOVIE GM
                        ON GM.FK_MOVIE_ID = M.ID
                        JOIN GENRE G
                        ON GM.FK_GENRE_ID = G.ID
                        JOIN PRODUCER_MOVIE PM
                        ON PM.FK_MOVIE_ID = M.ID
                        JOIN PRODUCER P
                        ON PM.FK_PRODUCER_ID = P.ID
                        WHERE P.ID = @id";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@id", producerId);

            List<Movie> result = new List<Movie>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Movie movie = new Movie();
                        movie.Id = reader.GetInt32(0);
                        movie.Name = reader.GetString(1);
                        movie.Country = GetValueOrNull<string>(reader, 2);
                        movie.Duration = reader.GetInt32(4);
                        movie.ReleaseDate = reader.GetDateTime(3);
                        movie.Genres = GetMovieGenres(movie.Id).ToList();
                        movie.Producers = GetMovieProducers(movie.Id).ToList();
                        result.Add(movie);
                    }
                }
            }

            return result;
        }

        public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName.ToUpper());
        }

        // +
        public double GetPriceByTimeAllowance(int id)
        {
            var query = @"select PRICE from TIME_ALLOWANCE where ID = @id";

            var command = _sqlConnection.CreateCommand();
            command.CommandText = query;
            command.Parameters.AddWithValue("@id", id);

            return (System.Single)command.ExecuteScalar();
        }

        // +
        public int? GetProducerIdByName(string name)
        {
            var query = @"SELECT P.ID FROM PRODUCER P
                          WHERE P.NAME = @NAME";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@NAME", name);

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        return reader.GetInt32(0);
                    }
                }
            }

            return null;
        }

        // +

        // +
        public IEnumerable<Session> GetSessionsByDate(string date)
        {
            var query = @"SELECT S.ID, S.FK_HALL_ID, M.ID, M.NAME, M.DURATION, S.DATE_, S.PRICE
                          FROM SESSION_ S
                          JOIN MOVIE M
                          ON S.FK_MOVIE_ID = M.ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            //sqlCommand.Parameters.AddWithValue("@DATE_", date);

            List<Session> result = new List<Session>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Session session = new Session();
                        session.Id = reader.GetInt32(0);
                        session.FK_HallId = reader.GetInt32(1);
                        session.MovieId = reader.GetInt32(2);
                        session.MovieName = reader.GetString(3);
                        session.Duration = reader.GetInt32(4);
                        session.StartTime = reader.GetDateTime(5).Date;
                        session.Price = reader.GetFloat(6);
                        string dt = session.StartTime.ToString("d");
                        if (dt == date)
                            result.Add(session);
                    }
                }
            }

            return result;
        }

        // +
        public IEnumerable<Session> GetSessionsByMovie(int movieId)
        {
            var query = @"SELECT S.ID, S.FK_HALL_ID, M.ID, M.NAME, M.DURATION, S.DATE_
                          FROM SESSION_ S
                          JOIN MOVIE M
                          ON S.FK_MOVIE_ID = M.ID 
                          WHERE M.ID = @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@ID", movieId);

            List<Session> result = new List<Session>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Session session = new Session();
                        session.Id = reader.GetInt32(0);
                        session.FK_HallId = reader.GetInt32(1);
                        session.MovieId = reader.GetInt32(2);
                        session.MovieName = reader.GetString(3);
                        session.Duration = reader.GetInt32(4);
                        session.StartTime = reader.GetDateTime(5);
                        result.Add(session);
                    }
                }
            }

            return result;
        }

        private static T GetValueOrNull<T>(DbDataReader reader, int ordinal) where T : class
        {
            return !reader.IsDBNull(ordinal) ? reader.GetFieldValue<T>(ordinal) : null;
        }

        public static T? GetValueOrNullable<T>(SqlDataReader reader, int ordinal) where T : struct
        {
            if (reader.IsDBNull(ordinal)) return null;
            return reader.GetFieldValue<T>(ordinal);
        }

        // +
        public int? GetSubscriptionByTicketId(int id)
        {
            var query = @"SELECT FK_SUBSCRIPTION_ID from TICKET where ID = @id";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@id", id);

            var reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            { return (int)GetValueOrNullable<decimal>(reader, 0); }
            return null;
        }

        // +
        public IEnumerable<SubscriptionType> GetSubscriptionTypes()
        {
            var query = @"SELECT ID, PRICE, SESSIONS_COUNT
                        FROM SUBSCRIPTION_TYPE WHERE DEACTIVATED = 0";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;

            List<SubscriptionType> result = new List<SubscriptionType>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        SubscriptionType subscriptionType = new SubscriptionType();
                        subscriptionType.Id = reader.GetInt32(0);
                        subscriptionType.Price = reader.GetFloat(1);
                        subscriptionType.SessionCount = reader.GetInt32(2);
                        result.Add(subscriptionType);
                    }
                }
            }

            return result;
        }

        // +
        public TimeAllowance GetTimeAllowance(DateTime dateTime)
        {
            var query = @"SELECT ID, PRICE, START_TIME, END_TIME 
                        FROM TIME_ALLOWANCE
                        WHERE START_TIME <= @TIME AND END_TIME >= @TIME_";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@TIME_", dateTime);

            TimeAllowance timeAllowance = new TimeAllowance();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        timeAllowance.Price = reader.GetFloat(1);
                        timeAllowance.Id = reader.GetInt32(0);
                        timeAllowance.StartTime = reader.GetDateTime(2);
                        timeAllowance.EndTime = reader.GetDateTime(3);
                        return timeAllowance;
                    }
                }
            }

            return null;
        }

        // +
        public User GetUserByName(string name)
        {
            var query = @"SELECT ID, USERNAME, PASSWORD, LAST_NAME, FIRST_NAME, ACCESS_TYPE, ACCOUNT_STATUS
                        FROM USER_
                        WHERE lower(USERNAME) = @USERNAME";//надо погуглить как искать с IgnoreCase

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@USERNAME", name.ToLower());

            User user = new User();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        user.Id = reader.GetInt32(0);
                        user.UserName = reader.GetString(1);
                        user.Password = reader.GetString(2);
                        user.FirstName = GetValueOrNull<string>(reader, 4);
                        user.LastName = GetValueOrNull<string>(reader, 3);
                        user.AccessType = reader.GetInt32(5);
                        user.AccountStatus = reader.GetInt32(6);
                        return user;
                    }
                }
            }

            return null;
        }

        public Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id.ToString());
        }

        public Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        // +
        public IEnumerable<Subscription> GetUserSubscriptions(int userId)
        {
            var query = @"SELECT s.ID, s.PRICE, BALANCE, s.FK_SUBSCRIPTION_TYPE_ID, st.SESSIONS_COUNT scount FROM SUBSCRIPTION s
                            join SUBSCRIPTION_TYPE st on 
                            s.FK_SUBSCRIPTION_TYPE_ID = st.ID
                        WHERE s.FK_USER_ID = @USERID and s.DEACTIVATED = 0";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@USERID", userId);

            List<Subscription> result = new List<Subscription>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Subscription subscription = new Subscription();
                        subscription.Id = reader.GetInt32(0);
                        subscription.Price = reader.GetFloat(1);
                        subscription.Balance = reader.GetInt32(2);
                        subscription.AllSessionsAmount = reader.GetInt32(4);
                        subscription.FK_UserId = userId;
                        subscription.FK_SubscriptionTypeId = reader.GetInt32(3);
                        result.Add(subscription);
                    }
                }
            }

            return result;
        }

        // +
        public User GetUserById(int id)
        {
            var query = @"SELECT ID, USERNAME, PASSWORD, LAST_NAME, FIRST_NAME, ACCESS_TYPE, ACCOUNT_STATUS
                        FROM USER_
                        WHERE ID = @id";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@id", id);

            User user = new User();

            using (var reader = sqlCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    user.Id = reader.GetInt32(0);
                    user.UserName = reader.GetString(1);
                    user.Password = reader.GetString(2);
                    user.FirstName = GetValueOrNull<string>(reader, 4);
                    user.LastName = GetValueOrNull<string>(reader, 3);
                    user.AccessType = reader.GetInt32(5);
                    user.AccountStatus = reader.GetInt32(6);
                    return user;
                }
            }

            return null;
        }

        // +
        public IEnumerable<Ticket> GetUserTickets(int userId)
        {
            var query = @"SELECT T.ID, S.FK_HALL_ID, P.ROW_, P.PLACE, M.NAME, T.BUYING_TIME, S.ID, P.ID, T.FK_SUBSCRIPTION_ID, T.PRICE
                        FROM TICKET T
                        JOIN SESSION_ S ON T.FK_SESSION_ID = S.ID
                        JOIN PLACE P
                        ON T.FK_PLACE_ID = P.ID
                        JOIN MOVIE M
                        ON S.FK_MOVIE_ID = M.ID
                        WHERE T.FK_USER_ID = @USERID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@USERID", userId);

            List<Ticket> result = new List<Ticket>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Ticket ticket = new Ticket();
                        ticket.Id = reader.GetInt32(0);
                        ticket.FK_HallId = reader.GetInt32(1);
                        ticket.Row = reader.GetInt32(2);
                        ticket.Place = reader.GetInt32(3);
                        ticket.MovieName = reader.GetString(4);
                        ticket.StartDate = reader.GetDateTime(5);
                        ticket.FK_SessionID = reader.GetInt32(6);
                        ticket.FK_PlaceID = reader.GetInt32(7);
                        ticket.Price = reader.GetFloat(9);
                        ticket.FK_UserId = userId;
                        ticket.FK_SubscriptionId = (int?)GetValueOrNullable<int>(reader, 8);
                        result.Add(ticket);
                    }
                }
            }

            return result;
        }

        // +
        public void IncreaseSessionsCount(int subscriptionId)
        {
            UpdateSubscription(subscriptionId, 1);
        }

        //для авторизации надо добавить отдельно столбец, где хранится uppercase имя пользователя
        public async Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
        {
            user.NormUsername = normalizedName;
        }

        public async Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
        {
            user.UserName = userName;
        }

        public Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
        {
            UpdateUser(user);
            return Task.FromResult(IdentityResult.Success);
        }

        // +
        public void UpdateMovie(Movie movie)
        {
            var query = @"UPDATE MOVIE SET NAME = @NEW_NAME, COUNTRY = @NEW_COUNTRY, DURATION = @NEW_DURATION, PRICE = @NEW_PRICE, DATE_ = @NEW_DATE_ 
                        WHERE ID = @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;

            sqlCommand.Parameters.AddWithValue("@NEW_NAME", movie.Name);
            sqlCommand.Parameters.AddWithValue("@NEW_COUNTRY", movie.Country);
            sqlCommand.Parameters.AddWithValue("@NEW_DURATION", movie.Duration);
            sqlCommand.Parameters.AddWithValue("@NEW_PRICE", movie.Price);
            sqlCommand.Parameters.AddWithValue("@NEW_DATE_", movie.ReleaseDate);
            sqlCommand.Parameters.AddWithValue("@ID", movie.Id);

            sqlCommand.ExecuteNonQuery();


            var prev = GetMovieById(movie.Id);

            foreach (var producer in prev.Producers)
            {
                if (!movie.Producers.Contains(producer))
                {
                    var prodInsert = @"DELETE FROM PRODUCER_MOVIE WHERE FK_MOVIE_ID = @ID and FK_PRODUCER_ID = @PRODUCER_ID";
                    var prodCom = _sqlConnection.CreateCommand();
                    prodCom.CommandText = prodInsert;
                    prodCom.Parameters.AddWithValue("@ID", movie.Id);
                    prodCom.Parameters.AddWithValue("@PRODUCER_ID", producer.Id);


                    prodCom.ExecuteNonQuery();
                }
            }

            foreach (var producer in movie.Producers)
            {
                if (!prev.Producers.Contains(producer))
                {
                    var prodInsert = @"INSERT INTO PRODUCER_MOVIE(FK_MOVIE_ID, FK_PRODUCER_ID) VALUES(@FK_MOVIE_ID, @FK_PRODUCER_ID)";
                    var prodCom = _sqlConnection.CreateCommand();
                    prodCom.CommandText = prodInsert;
                    prodCom.Parameters.AddWithValue("@FK_MOVIE_ID", movie.Id);
                    prodCom.Parameters.AddWithValue("@FK_PRODUCER_ID", producer.Id);


                    prodCom.ExecuteNonQuery();
                }
            }

            foreach (var genre in prev.Genres)
            {
                if (!movie.Genres.Contains(genre))
                {
                    var genreInsert = @"DELETE FROM GENRE_MOVIE WHERE FK_MOVIE_ID = @ID and FK_GENRE_ID = @GENRE_ID";
                    var genreCom = _sqlConnection.CreateCommand();
                    genreCom.CommandText = genreInsert;
                    genreCom.Parameters.AddWithValue("@ID", movie.Id);
                    genreCom.Parameters.AddWithValue("@GENRE_ID", genre.Id);


                    genreCom.ExecuteNonQuery();
                }
            }

            foreach (var genre in movie.Genres)
            {
                if (!prev.Genres.Contains(genre))
                {
                    var genreInsert = @"INSERT INTO GENRE_MOVIE(FK_MOVIE_ID, FK_GENRE_ID) VALUES(@FK_MOVIE_ID, @FK_GENRE_ID)";
                    var genreCom = _sqlConnection.CreateCommand();
                    genreCom.CommandText = genreInsert;
                    genreCom.Parameters.AddWithValue("@FK_MOVIE_ID", movie.Id);
                    genreCom.Parameters.AddWithValue("@FK_GENRE_ID", genre.Id);


                    genreCom.ExecuteNonQuery();
                }
            }

        }

        //// +
        //public void UpdateSubscriptionType(SubscriptionType subscriptionType)
        //{
        //    var query = @"UPDATE SUBSCRIPTION_TYPE SET SESSION_COUNT = :NEW_SESSION_COUNT, PRICE = :NEW_PRICE, IS_ACTIVE = :NEW_IS_ACTIVE 
        //                WHERE ID = :ID";

        //    var sqlCommand = _sqlConnection.CreateCommand();
        //    sqlCommand.CommandText = query;
        //    sqlCommand.Parameters.AddWithValue("ID", subscriptionType.Id);
        //    sqlCommand.Parameters.AddWithValue("NEW_SESSION_COUNT", subscriptionType.SessionCount);
        //    sqlCommand.Parameters.AddWithValue("NEW_PRICE", subscriptionType.Price);
        //    sqlCommand.Parameters.AddWithValue("NEW_IS_ACTIVE", subscriptionType.Deactivated);

        //    sqlCommand.ExecuteNonQuery();
        //}

        // +
        public void UpdateTimeAllowance(TimeAllowance timeAllowance)
        {
            var query = @"UPDATE TIME_ALLOWANCE SET START_TIME = @NEW_START_TIME, END_TIME = @NEW_END_TIME, PRICE = @NEW_PRICE 
                        WHERE ID = @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;

            sqlCommand.Parameters.AddWithValue("@NEW_START_TIME", timeAllowance.StartTime);
            sqlCommand.Parameters.AddWithValue("@NEW_END_TIME", timeAllowance.EndTime);
            sqlCommand.Parameters.AddWithValue("@NEW_PRICE", timeAllowance.Price);
            sqlCommand.Parameters.AddWithValue("@ID", timeAllowance.Id);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public void UpdateUser(User user)
        {
            //ACCESS_TYPE = :NEW_ACCESS_TYPE, ACCOUNT_STATUS = :NEW_ACCOUNT_STATUS 
            var query = @"UPDATE USER_ SET LAST_NAME= @NEW_LAST_NAME, PASSWORD = @NEW_PASSWORD, FIRST_NAME = @NEW_FIRST_NAME where ID= @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@NEW_LAST_NAME", (object)user.LastName ?? DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NEW_PASSWORD", user.Password);
            sqlCommand.Parameters.AddWithValue("@NEW_FIRST_NAME", (object)user.FirstName ?? DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@ID", user.Id);
            //sqlCommand.Parameters.AddWithValue("@NEW_USERNAME", user.UserName);



            //sqlCommand.Parameters.AddWithValue("@NEW_ACCESS_TYPE", user.AccessType);
            //sqlCommand.Parameters.AddWithValue("@NEW_ACCOUNT_STATUS", user.AccountStatus);

            sqlCommand.ExecuteNonQuery();
        }

        // +
        public void DeactivateUser(int id)
        {
            var query = @"update USER_ set ACCOUNT_STATUS = 0 where ID = @id";
            var com = _sqlConnection.CreateCommand();
            com.CommandText = query;
            com.Parameters.AddWithValue("@id", id);

            com.ExecuteNonQuery();
        }

        // +
        public IEnumerable<Log> GetUserLog(int userId)
        {
            var query = @"SELECT L.ID, L.OLD_VALUE, L.DATE_, U.USERNAME, A.DESCRIPTION FROM LOG_ L 
                        JOIN USER_ U
                        ON U.ID = L.FK_USER_ID
                        JOIN ACTION A
                        ON A.ID = L.FK_ACTION_ID
                        WHERE L.FK_USER_ID = @USER_ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@USER_ID", userId);

            List<Log> result = new List<Log>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Log log = new Log();
                        log.Id = reader.GetInt32(0);
                        log.OldValue = GetValueOrNull<string>(reader, 1);
                        log.Date = reader.GetDateTime(2);
                        log.UserName = reader.GetString(3);
                        log.ActionName = reader.GetString(4);
                        result.Add(log);
                    }
                }
            }

            return result;
        }

        public async Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
        {
            user.Password = passwordHash;
        }

        public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Password);
        }

        public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(!string.IsNullOrWhiteSpace(user.Password));
        }

        // +
        public Subscription GetSubscriptionById(int subscriptionId)
        {
            var query = @"SELECT ID, PRICE, BALANCE, s.FK_SUBSCRIPTION_TYPE_ID, st.SESSIONS_COUNT scount, FK_USER_ID FROM SUBSCRIPTION s
                            join SUBSCRIPTION_TYPE st on 
                            s.FK_SUBSCRIPTION_TYPE_ID = st.ID
                        WHERE s.ID = @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@ID", subscriptionId);

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Subscription subscription = new Subscription();
                        subscription.Id = reader.GetInt32(0);
                        subscription.Price = reader.GetFloat(1);
                        subscription.Balance = reader.GetInt32(2);
                        subscription.AllSessionsAmount = reader.GetInt32(4);
                        subscription.FK_UserId = reader.GetInt32(5);
                        subscription.FK_SubscriptionTypeId = reader.GetInt32(3);
                        return subscription;
                    }
                }
            }

            return null;
        }

        // +
        public void UpdateSubscription(int subscriptionId, int add)
        {
            var query = @"UPDATE SUBSCRIPTION SET BALANCE = BALANCE + @ADD_
                        WHERE ID = @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@ADD_", add);
            sqlCommand.Parameters.AddWithValue("@ID", subscriptionId);

            sqlCommand.ExecuteNonQuery();
        }

        public IEnumerable<Session> GetAllSessions()
        {
            var query = @"SELECT S.ID, S.FK_HALL_ID, M.ID, M.NAME, M.DURATION, S.DATE_, S.PRICE
                          FROM SESSION_ S
                          JOIN MOVIE M
                          ON S.FK_MOVIE_ID = M.ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;

            List<Session> result = new List<Session>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Session session = new Session();
                        session.Id = reader.GetInt32(0);
                        session.FK_HallId = reader.GetInt32(1);
                        session.MovieId = reader.GetInt32(2);
                        session.MovieName = reader.GetString(3);
                        session.Duration = reader.GetInt32(4);
                        session.StartTime = reader.GetDateTime(5);
                        session.Price = reader.GetFloat(6);
                        result.Add(session);
                    }
                }
            }

            return result;
        }

        public IEnumerable<Genre> GetMovieGenres(int movieId)
        {
            var query = @"SELECT G.ID, G.NAME FROM GENRE G
                        INNER JOIN GENRE_MOVIE GM ON GM.FK_GENRE_ID = G.ID WHERE GM.FK_MOVIE_ID = @FK_MOVIE_ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@FK_MOVIE_ID", movieId);

            List<Genre> result = new List<Genre>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Genre genre = new Genre();
                        genre.Id = reader.GetInt32(0);
                        genre.Name = reader.GetString(1);
                        result.Add(genre);
                    }
                }
            }

            return result;
        }

        public Genre GetGenreById(int id)
        {
            var query = @"SELECT G.ID, G.NAME
                        FROM GENRE G
                        WHERE G.ID = @id";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@id", id);

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Genre genre = new Genre();
                        genre.Id = reader.GetInt32(0);
                        genre.Name = reader.GetString(1);
                        return genre;
                    }
                }
            }

            return null;
        }

        public Producer GetProducerById(int id)
        {
            var query = @"SELECT P.ID, P.NAME
                        FROM PRODUCER P
                        WHERE P.ID = @id";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@id", id);

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Producer producer = new Producer();
                        producer.Id = reader.GetInt32(0);
                        producer.Name = reader.GetString(1);
                        return producer;
                    }
                }
            }

            return null;
        }

        public void DeleteSubscriptionType(int id)
        {
            var query = @"UPDATE SUBSCRIPTION_TYPE set DEACTIVATED = 1 where ID = @id";
            var com = _sqlConnection.CreateCommand();
            com.CommandText = query;
            com.Parameters.AddWithValue("@id", id);

            com.ExecuteNonQuery();
        }

        public IEnumerable<Ticket> GetSessionTickets(int sessionid)
        {
            var query = @"SELECT T.ID, SS.ID
                        FROM TICKET T
                        JOIN SESSION_ S ON T.FK_SESSION_ID = S.ID
                        left outer join SUBSCRIPTION SS
                        on SS.ID = T.FK_SUBSCRIPTION_ID 
                        WHERE S.ID = @ID";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@ID", sessionid);

            var res = new List<Ticket>();

            using (var reader = sqlCommand.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Ticket t = new Ticket();
                        t.Id = reader.GetInt32(0);
                        t.FK_SubscriptionId = (int)GetValueOrNullable<decimal>(reader, 1);
                        res.Add(t);
                    }

                    return res;
                }
            }

            return Enumerable.Empty<Ticket>();
        }

        public void DeleteMovieSessions(int movieid)
        {
            var query = @"delete from SESSION_ where ID in(SELECT S.ID
                          FROM SESSION_ S
                          JOIN MOVIE M
                          ON S.FK_MOVIE_ID = M.ID 
                          WHERE M.ID = @ID)";

            var sqlCommand = _sqlConnection.CreateCommand();
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@ID", movieid);

            sqlCommand.ExecuteNonQuery();

        }

        //public void DeleteMovieTickets(int movieid)
        //{
        //    var query = @"delete from TICKETS where ID in(SELECT T.ID
        //                  FROM TICKET T
        //                  JOIN SESSION_ S
        //                  ON T.FK_SESSION_ID = S.ID
        //                  JOIN MOVIE M
        //                    on M.ID = S.FK_MOVIE_ID
        //                  WHERE M.ID = @ID)";

        //    var sqlCommand = _sqlConnection.CreateCommand();
        //    sqlCommand.CommandText = query;
        //    sqlCommand.Parameters.AddWithValue("@ID", movieid);

        //    sqlCommand.ExecuteNonQuery();
        //}

        public IEnumerable<Ticket> GetMovieTickets(int movieid)
        {
            var query = @"select T.ID, SS.ID from
                          TICKET T
                join SESSION S
                on S.ID = T.FK_SESSION_ID
                join MOVIE M
                on M.ID = S.FK_MOVIE_ID
                left outer join SUBSCRIPTION SS
                on SS.ID = T.FK_SUBSCRIPTION_ID
                where M.ID = @ID";

            var com = _sqlConnection.CreateCommand();
            com.CommandText = query;
            com.Parameters.AddWithValue("@ID", movieid);

            var res = new List<Ticket>();

            using (var reader = com.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Ticket t = new Ticket();
                        t.Id = reader.GetInt32(0);
                        t.FK_SubscriptionId = (int)GetValueOrNullable<decimal>(reader, 1);
                        res.Add(t);
                    }

                    return res;
                }
            }

            return Enumerable.Empty<Ticket>();
        }
    }
}
