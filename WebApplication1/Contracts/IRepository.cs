﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cinema.DataModels;

namespace Cinema.Contracts
{
    public interface IRepository : IUserStore<User>, IUserPasswordStore<User>
    {

        Movie GetMovieById(int id);
        IEnumerable<Movie> GetMoviesByNameGenreOrProducer(string name, int genre, int producer);   
        IEnumerable<Movie> GetCurrentMovies(DateTime now, string name, int genreid, int prodid);

        void DeleteSubscriptionType(int id);

        IEnumerable<Producer> GetMovieProducers(int movieId);

        IEnumerable<Session> GetSessionsByDate(string date);
        IEnumerable<Session> GetSessionsByMovie(int movieId);

        IEnumerable<int> GetAllHalls();

        TimeAllowance GetTimeAllowance(DateTime dateTime);

        IEnumerable<Place> GetFreePlacesForSession(int sessionId);

        IEnumerable<Ticket> GetUserTickets(int userId);

        IEnumerable<Subscription> GetUserSubscriptions(int userId);
        Subscription GetSubscriptionById(int subscriptionId);

        IEnumerable<SubscriptionType> GetSubscriptionTypes();
        IEnumerable<Genre> GetGenres();
        Genre GetGenreById(int id);
        IEnumerable<Genre> GetMovieGenres(int movieId);

        IEnumerable<Producer> GetProducers();

        User GetUserByName(string name);

        void AddMovie(Movie movie);
        void UpdateMovie(Movie movie);
        void DeleteMovie(int id);

        int AddProducer(string name);
        int? GetProducerIdByName(string name);
        Producer GetProducerById(int id);

        int? GetGenreIdByName(string name);

        void AddTimeAllowance(TimeAllowance timeAllowance);
        void UpdateTimeAllowance(TimeAllowance timeAllowance);

        void AddSession(Session session);
        void DeleteSession(int id);

        void AddUser(User user);
        void UpdateUser(User user);

        void AddSubscriptionType(SubscriptionType subscriptionType);
       // void UpdateSubscriptionType(SubscriptionType subscriptionType);

        void AddSubscription(Subscription subscription);
        void IncreaseSessionsCount(int subscriptionId);
        void DecreaseSessionsCount(int subscriptionId);
        void UpdateSubscription(int subscriptionId, int add);
        void DeleteSubscription(int id);

        void AddTicket(Ticket ticket);
        void DeleteTicket(int id);

        double CalculatePrice(int sessionid, int placeid);
        double GetPriceByTimeAllowance(int id);

        int? GetSubscriptionByTicketId(int id);
        User GetUserById(int id);

        void DeactivateUser(int id);

        IEnumerable<Log> GetUserLog(int userid);
        IEnumerable<Session> GetAllSessions();
        IEnumerable<Ticket> GetSessionTickets(int sessionid);
        void DeleteMovieSessions(int movieid);
        //void DeleteMovieTickets(int movieid);
        IEnumerable<Ticket> GetMovieTickets(int movieid);

    }
}
