﻿using Cinema.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.ViewComponents
{
    public class RowAndPlaceSelectorViewComponent : ViewComponent
    {
        private readonly IRepository _repo;
        public RowAndPlaceSelectorViewComponent(IRepository repo)
        {
            _repo = repo;
        }
        public async Task<IViewComponentResult> InvokeAsync(int sessionId)
        {
            var places = _repo.GetFreePlacesForSession(sessionId);
            return View("_RowAndPlaceSelector.cshtml", places.ToList());
        }
    }
}
