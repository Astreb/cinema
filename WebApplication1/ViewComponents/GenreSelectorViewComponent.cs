﻿using Cinema.Contracts;
using Cinema.DataModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.ViewComponents
{
    public class GenreSelectorViewComponent : ViewComponent
    {
        private readonly IRepository _repo;
        public GenreSelectorViewComponent(IRepository repo)
        {
            _repo = repo;
        }
        public async Task<IViewComponentResult> InvokeAsync(List<Genre> movieGenres)
        {
            var genres = _repo.GetGenres();
            var res = genres.ToList();
            foreach(var g in res)
            {
                if(movieGenres!= null && movieGenres.Any( x => x.Id == g.Id))
                {
                    g.Selected = true;
                }
            }
            return View("_GenreSelector.cshtml", res);
        }
    }
}
