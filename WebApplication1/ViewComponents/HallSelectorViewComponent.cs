﻿using Cinema.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.ViewComponents
{
    public class HallSelectorViewComponent : ViewComponent
    {
        private readonly IRepository _repo;
        public HallSelectorViewComponent(IRepository repo)
        {
            _repo = repo;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var halls = _repo.GetAllHalls();
            return View("_HallSelector.cshtml", halls.ToList());
        }
    }
}
