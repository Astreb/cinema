﻿using Cinema.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.ViewComponents
{
    public class SubscriptionTypeSelectorViewComponent : ViewComponent
    {
        private readonly IRepository _repo;
        public SubscriptionTypeSelectorViewComponent(IRepository repo)
        {
            _repo = repo;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var types = _repo.GetSubscriptionTypes();
            return View("_SubscriptionTypeSelector.cshtml", types.ToList());
        }
    }
}
