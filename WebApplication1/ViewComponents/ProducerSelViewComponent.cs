﻿using Cinema.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.ViewComponents
{
    public class ProducerSelViewComponent : ViewComponent
    {
        private readonly IRepository _repo;
        public ProducerSelViewComponent(IRepository repo)
        {
            _repo = repo;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var producers = _repo.GetProducers();
            var res = producers.ToList();
            res.Insert(0, new DataModels.Producer { Id = 0, Name = "None" });
            return View("_ProducerSel.cshtml", res);
        }
    }
}
