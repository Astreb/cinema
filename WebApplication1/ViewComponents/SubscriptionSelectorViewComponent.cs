﻿using Cinema.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.ViewComponents
{
    public class SubscriptionSelectorViewComponent : ViewComponent
    {
        private readonly IRepository _repo;
        public SubscriptionSelectorViewComponent(IRepository repo)
        {
            _repo = repo;
        }
        public async Task<IViewComponentResult> InvokeAsync(int userid)
        {
            var subscriptions = _repo.GetUserSubscriptions(userid);
            var res = subscriptions.ToList();
            res.Insert(0, new DataModels.Subscription { Id = 0 });
            return View("~/Views/Ticket/_SubscriptionSelector.cshtml", res);
        }
    }
}
