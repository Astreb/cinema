﻿using Cinema.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.ViewComponents
{
    public class GenreSelViewComponent : ViewComponent
    {
        private readonly IRepository _repo;
        public GenreSelViewComponent(IRepository repo)
        {
            _repo = repo;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var genres = _repo.GetGenres();
            var res = genres.ToList();
            res.Insert(0, new DataModels.Genre { Id = 0, Name = "None" });
            return View("_GenreSel.cshtml", res);
        }
    }
}
