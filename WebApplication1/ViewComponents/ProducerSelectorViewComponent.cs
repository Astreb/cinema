﻿using Cinema.DataModels;
using Cinema.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cinema.ViewComponents
{
    public class ProducerSelectorViewComponent : ViewComponent
    {
        private readonly IRepository _repo;
        public ProducerSelectorViewComponent(IRepository repo)
        {
            _repo = repo;
        }
        public async Task<IViewComponentResult> InvokeAsync(List<Producer> movieProducers)
        {
            var producers = _repo.GetProducers();
            var res = producers.ToList();
            //res.Insert(0, new DataModels.Producer { Id = 0, Name = "None" });
            foreach (var p in res)
            {
                if (movieProducers != null && movieProducers.Any(x => x.Id == p.Id))
                {
                    p.Selected = true;
                }
            }
            return View("_ProducerSelector.cshtml", res);
        }
    }
}
